Create `/code` folder.

Place your craft installation in `/code` folder.

Rename `/code/public` to `/code/public_html`.

run `docker-compose up -d`

Port mapping:

- 8080:80
- 8443:443